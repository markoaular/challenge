FROM python:3.7-alpine

ENV FLASK_APP main.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 8080 
ENV FLASK_ENV development
ENV FLASK_DEBUG 1
WORKDIR /
COPY . .
RUN pip install flask==1.0.2
EXPOSE 8080

CMD ["flask", "run"]
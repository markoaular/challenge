# Challenge

-  Dado un repository hosteado en GitLab que sea privado
-  Dado un tag creado para un commit específico:  
-  Se debe crear una imágen Docker 
-  Se debe pushear la imágen creada al registry de gitlab


**Constraints**:

- El repository debe ser privado
- Las **únicas** herramientas que pueden utilizarse son GitLab Cloud (todas las tools que ofrece).
- El candidato debe darnos permisos tanto para el repository como para la infra seleccionada.
- Si el candidato no tiene fácil acceso a un cloud provider o al servicio que él escoja, se lo facilitaremos.
- La imagen de docker debe contener como label el tag del commit.
- Debe correr la imagen de docker generada en una instancia. 

# Flask Application

Flask es un framework de aplicación web WSGI ligero. Está diseñado para que la puesta en marcha sea rápida y sencilla, con la capacidad de escalar a aplicaciones complejas. Comenzó como un simple wrap de Werkzeug y Jinja y se ha convertido en uno de los frameworks de aplicaciones web de Python más populares.



## Prerequisites

	1. Python 3.7
	2. Flask 1.0.2
    
    Al instalar Flask se instalaran automaticamente sus dependencias:

        - Werkzeug: implementa WSGI, la interfaz estándar de Python entre aplicaciones y servidores.
        - Jinja:    es un lenguaje de plantilla que representa las páginas que atiende su aplicación.
        - MarkupSafe: viene con Jinja. Al renderizar plantillas  evita ataques de inyección a traves de inputs.
        - ItsDangerous: firma datos de forma segura para garantizar su integridad. Esto se usa para proteger la cookie de sesión de Flask.
        - Click: es un framework para escribir aplicaciones de línea de comandos. Proporciona el comando flask y permite agregar comandos de 
          administración    personalizados.
    
    3. Python pip
	
	
## Instrucciones para correr esta aplicación

    - git clone https://gitlab.com/markoaular/challenge.git
    - cd /challenge
    - Instalar requirements.txt ejecutando pip install requirements.txt
    - Ejecutar `python main.py` / otra opcion es ejecutar `FLASK_ENV=development flask run`




# Dockerizar aplicacion

## Crear el Dockerfile con los archivos necesarios para generar la imagen (codigo de la app y requirements)

```
FROM python:3.7-alpine

ENV FLASK_APP main.py
ENV FLASK_RUN_HOST 0.0.0.0
ENV FLASK_RUN_PORT 8080 
ENV FLASK_ENV development
ENV FLASK_DEBUG 1
WORKDIR /
COPY . .
RUN pip install requirements.txt
EXPOSE 8080

CMD ["FLASK_ENV=development", "flask", "run"]
```


Como registry utilizaremos la que nos proporciona gitlab registry.gitlab.com/markoaular/challenge

En nuestro .gitlab-ci.yml tendremos un stage build/push, en este stage se hara el build de la image a partir del Dockerfile mostrado arriba y luego se hara el push a nuestra registry.

Para la contruccion de la imagen utilizaremos dentro de nuestro stage una dockerimage llamada docker que a su vez tiene el servicio docker (dockerindocker) corriendo dentro. al ser una imagen liviana y con proceso de docker basico nos permite ganar mas velocidad al momento de construir nuestra imagen, para etiquetar nuestra iamgen docker defini variables de entorno dentro del pipeline para armar un tag personalizado, en este caso se etiquetara con el SHA del commit, de manera que cada vez que hagamos un commit en nuestra applicacion quedara etiquetada con ese `COMMIT_SHA` especifico, ademas le agregara el nombre del proyecto como parte de personalizacion de la etiqueta. el formato quedaria `<CI_REGISTRY_IMAGE:CI_COMMIT_SHORT_SHA>`, aunado a este proceso tambien hara un push de la imagen  con el nombre del branch MASTER, en el contexto de que trabajemos en ambientes de produccion, nos permitira identificar y desplegar la imagen final correcta.

Durante el proceso del build, tambien añadi el metodo "`--from-cache`" de docker, esto me permite utilizar capas  de imagenes creadas anteriormente en el proceso que quedaron en cache o memoria temporal, esto para optimizar y agilizar el proceso de build.



Cree un branch llamado DEV, donde esta aplicacion sera desplegada como contenedor en un cloudserver, en este servidor estara el servicio de Docker instalado y corriendo, este server tiene Ubuntu 18.04 instalado, se instalo el servicio ssh y tambien se abrio en las reglas de firewall el puerto 0.0.0.0/0:8080 para permitir el acceso a nuestra aplicacion desde cualquier direccion ip, la imagen desplegada en este servidor sera la etiquetada con el ID del Commit. 

![](/images/imagenregistry.JPG)





Para que gitlab puede desplegar la imagen de la app en mi cloudserver, se deben definir variables de entornos que tendran como valores los datos de conexion a nuestro cloudserver, en este caso se definio la SSH_KEY, la IP del servidor y el USER para conectar por ssh.

para definir estas variables en gitlab debemos ir a configuracion del repositorio/CICD/Variables


![Variables](/images/variablescloudserver.JPG)

Link para probar el funcionamiento del contenedor

Link: http://66.97.46.54:8080          http://66.97.46.54:8080/test



![](/images/dockerflask_running.JPG)



![apprunning](/images/apprunning.JPG)                                        ![AppRunningTest](/images/apprunningtest.JPG)


la idea de hacer este flujo de despliegue es seguir con las mejores practicas de gitops, en este caso seria comprobar que nuestra app funcione bien con los cambios hechos en ella, luego de comprobar el buen funcionamiento se haria un merge request de los cambios luego de aprobado se procede a desplegar en produccion.


![PipelineDev to deploy Pre](/images/PipelineDev.JPG)   



Para la parte de despliegue en produccion se utilizara la Master Branch, paradesplegar en produccion desplegaremos nuestra app en un servicio de AWS llamado ECS (Elastic Container Service), es un servicio de administración de contenedores altamente escalable y rápido que facilita la tarea de ejecutar, detener y administrar contenedores de Docker en un clúster.

![Deploy Manual to Prod](/images/PipelineMaster.JPG)          ![AWS ECS](/images/ecs.JPG)


Para este despliegue se instalo un cusotom gitlab-runner con AWS-CLI instalado y configurado con nuestras credenciales de AWS (ACCESS_KEY_I, SECRET_ACCESS_KEY, REGION).

En la creacion de nuestro cluster ECS definiremos que utilice nuestra registry de gitlab para desplegar la imagen con la etiqueta master, esta sera nuestra imagen a utilizar en produccion.

En ECS seleccione como metodo de despliegue AWS FARGATE este  permite centrarse en la creación de  aplicaciones. Fargate elimina la necesidad de aprovisionar y administrar servidores, le permite especificarrecursos por aplicación y mejora la seguridad mediante el aislamiento de aplicaciones por diseño.
Fargate asigna la cantidad correcta de cómputo, lo que elimina la necesidad de elegir instancias y escalar la capacidad del clúster.


![AWS Fargate](/images/fargate.JPG)


## Nota: por tema de costos el servicio AWS ECS esta pausado, pueden solicitarme activarlo para ver el funcionamiento de este despliegue en especifico. 



          

	
